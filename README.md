# The PHP client library for Interledger
## The ILP module includes:
* Support integration with _**ILP SPSP Invoice Server**_ that allows to create and get invoices. 

## Setup development environment
### Start ILP invoice server and Moneyd
```
cd test
docker-compose up -d
```
### Run Tests
```
composer install --dev
vendor/bin/phpunit --configuration phpunit.xml
```

## How to use
To learn more about ILP SPSP Invoice Server click [here](https://github.com/interledgerjs/ilp-spsp-invoice-server#ilp-spsp-invoice-server).

### Create Invoice
```
$client = new ILPInvoiceServerClient('http://localhost:6000', 'test', false);
$amount = "900";
$assetCode = "XRP";
$assetScale = "9";
$webhook = "https://my.webhook.test"; # URL to be called after a successful payment
$response = $client->createInvoice($amount, $assetCode, $assetScale, $webhook, ["reason" => "more stuff"]);
```

### Get Invoice
```
# The parameter corresponds to a URL of an invoice.
$invoice = $client->getInvoice("http://localhost:6000/06bb8852-b6cb-45f4-ba99-7e8809903db1");
```